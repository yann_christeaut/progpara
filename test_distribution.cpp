#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include "communications.hpp"
#include "math.h"
// #include "calculs_matriciels.hpp"
using namespace std;


int main ( int argc , char **argv )
{
  int pid, nprocs;
  MPI_Status status;
  MPI_Init (&argc , &argv) ;
  MPI_Comm_rank(MPI_COMM_WORLD, &pid);
  MPI_Comm_size (MPI_COMM_WORLD, &nprocs);
  MPI_Comm Comm_grille;

  int N = atoi(argv[1]);
  int root = atoi(argv[2]);


  int nbprocs_dims[2];
  int period[2];
  nbprocs_dims[0]=sqrt(nprocs);
  nbprocs_dims[1]=sqrt(nprocs);
  period[0] = 0;
  period[1] = 0;
  // MPI_Dims_create(nprocs, 2, nbprocs_dims);
  MPI_Cart_create(MPI_COMM_WORLD, 2, nbprocs_dims, period, true, &Comm_grille);
  int moi[2];
  MPI_Cart_coords(Comm_grille, pid, 2, moi);


  cout << "i : " << nbprocs_dims[0] << ", j : " << nbprocs_dims[1] << endl;
  int n; // taille du bloc
  int x_p; // nombre de bloc x_p x x_p

  // initialisation permet de vérifier que les contraintes sont respectées
  // et permet de calculer la taille du bloc et le nombre de bloc en fonction du nombre de processeurs
  bool possible = initialisation(nbprocs_dims[0], nbprocs_dims[1], N, &n, &x_p);


  if (possible) {
    int p1 = nbprocs_dims[0];
    float* matrice;
    if (pid==root) {
      matrice = new float[N*N];
      srand(time(NULL));
      for (int i=0; i<N; i++)
	for (int j=0; j<N; j++)
	  matrice[i*N+j] = rand()%10;
    }

    int bloc_size = x_p*(n*n);
    float* bloc = new float[bloc_size];

    // fonction qui distribue à partir du processeur root la matrice par bloc élémentaire
    // à la fin de la fonction bloc contient tous les blocs de la matrice initiale attribués au processeur
    distributionN_to_n(matrice,bloc,N,n,x_p,root,p1,Comm_grille);

    float* matrice2;
    if (pid==root)
      matrice2 = new float[N*N];

    // fonction qui rassemble sur le processeur root tous les blocs pour retrouver la matrice
    rassemblementn_to_N(matrice2,bloc,N,n,x_p,root,p1,Comm_grille);
    if (pid==root) {
      float tmp = 0;
      for (int i=0; i<N*N; i++)
	tmp+=(matrice[i]-matrice2[i]);
      cout << "la somme des différences : " << tmp << endl;
    }


    if (pid == root) {
      delete[] matrice2;
      delete[] matrice;
    }
    delete[] bloc;
  }
  MPI_Finalize() ;
  return 0 ;
}
