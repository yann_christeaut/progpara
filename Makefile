MPICXX = mpic++
SRC= $(wildcard *.cpp)

all : test_distribution

%.o :%.cpp
	$(MPICXX) -o $@ -c $< 

test_distribution : test_distribution.o communications.o 
	$(MPICXX) -o $@ $^

.PHONY: clean mrproper

clean:
	@rm -rf *.o

mrproper: clean
	@rm -rf $(EXEC)
