#ifndef COMMUNICATIONS_HPP
#define COMMUNICATIONS_HPP

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

using namespace std;
bool initialisation(int p1, int p2, int N, int* n, int* x_p);
void distributionN_to_n(float* matrice, float* bloc, int N, int n, int x_p, int root, int p, MPI_Comm grille);
void rassemblementn_to_N(float* matrice, float* bloc, int N, int n, int x_p, int root, int p, MPI_Comm grille);
#endif
