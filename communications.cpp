#include "communications.hpp"

bool initialisation(int p1, int p2, int N, int* n, int* x_p)
{

  *x_p = p1*p2;

  *n = N/(*x_p);

  return (p1 == p2) && (N % p1 == 0) && (*x_p % p1 == 0);
}

void distributionN_to_n(float* matrice, float* bloc, int N, int n, int x_p, int root, int p, MPI_Comm grille)
{
}

void rassemblementn_to_N(float* matrice, float* bloc, int N, int n, int x_p, int root, int p, MPI_Comm grille)
{
  int pid, nprocs;
  MPI_Status status;
  MPI_Request request;
  MPI_Comm_rank(MPI_COMM_WORLD, &pid);
  MPI_Comm_size (MPI_COMM_WORLD, &nprocs);

  int blocs_size = x_p*(n*n);
  float* b = new float[blocs_size];

  if(pid == root){

    for(int proc = 0; proc < nprocs;proc++ ){
      if(proc != pid){
        MPI_Irecv(b,blocs_size,MPI_FLOAT,proc,0,grille,&status)
        MPI_Wait(&request, &status);
      }
      else{
        b = bloc
      }
      for( int i = 0; i<x_p ;i++){
        for(int j = 0; j<n;j++ ){
          for(int k = 0; k<n;k++ ){
            matrice[(j*N+k)+n*proc+n*p*i] = b[i*(n*n)+j*n+k];
          }
        }
      }
    }

  }
  else{
    MPI_Isend(bloc,blocs_size,MPI_FLOAT,root,0,grille,&request)
  }
}
